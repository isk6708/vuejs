import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import senaraiPengguna from '../views/pengguna/senaraiPengguna.vue'
import borangPengguna from '../views/pengguna/_formPengguna.vue'
import loginScreen from '../components/Login.vue'
import generalPublic from '../components/GeneralPublic.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/general-public',
      name: 'generalPublic',
      component: generalPublic
    },,
    {
      path: '/login',
      name: 'login',
      component: loginScreen
    },
    {
      path: '/senarai-pengguna-passport',
      name: 'senaraiPenggunaPassport',
      component: senaraiPengguna
    },{
      path: '/senarai-pengguna-jwt',
      name: 'senaraiPenggunaJwt',
      component: senaraiPengguna
      
     
  },
    {
      path: '/senarai-pengguna',
      name: 'senaraiPengguna',
      component: senaraiPengguna
    },
    {
      path: '/tambah-pengguna',
      name: 'tambahPengguna',
      component: borangPengguna
    },
    {
      path: '/kemaskini-pengguna/:id',
      name: 'kemaskiniPengguna',
      component: borangPengguna
    },
    // {
    //   path: '/',
    //   name: 'home',
    //   component: HomeView
    // },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (About.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/AboutView.vue')
    }
  ]
})

export default router
