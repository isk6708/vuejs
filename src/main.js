import { createApp } from 'vue'
import App from './App.vue'

// import Vue from 'vue'
// import Vuelidate from 'vuelidate'


import router from './router'
import 'bootstrap/dist/css/bootstrap.css';

// import './assets/main.css'

const app = createApp(App)

app.use(router)
// Vue.use(Vuelidate)

app.mount('#app1')

